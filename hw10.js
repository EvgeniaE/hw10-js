
  // 1 variant
// const getTabs = document.querySelectorAll(".tabs-title");
// const getTabsContent = document.querySelectorAll(".tab-content");
// getTabs.forEach((tab) =>
//   tab.addEventListener("click", () => {
//     const attr = tab.dataset.tab;
//     console.log(attr);
//     document.querySelector(".tabs-title.active").classList.remove("active");
//     tab.classList.add("active");

//     getTabsContent.forEach((content) => {
//       const attrContent = content.dataset.content;
//       console.log(attrContent);
//       if (attrContent === `${attr}-content`) {
//         content.classList.add("active");
//       } else if (attrContent !== `${attr}-content`) {
//         content.classList.remove("active");
//         content.classList.add("hidden");
//       }
//     });
//   })
// );

  // 2 variant
// const getTabs = document.querySelectorAll(".tabs-title");
//         const getTabsContent = document.querySelectorAll(".tab-content");
//         console.log(getTabsContent);

//         getTabs.forEach(tab => tab.addEventListener("click", () => {
//             const attr = tab.dataset.tab;

//             document.querySelector('.tabs-title.active').classList.remove('active');
//             tab.classList.add('active');

//             document.querySelector('.tab-content.active').classList.remove('active');
//             document.querySelector(`[data-content=${attr}-content]`).classList.add('active');
//         }));

// delegation
const tabsTitleList = document.querySelector('.tabs');

tabsTitleList.addEventListener('click', (event) => {
    if (event.target.classList.contains('tabs-title')) {
        const attr = event.target.dataset.tab;
        document.querySelector('.tabs-title.active').classList.remove('active');
        event.target.classList.add('active');
        document.querySelector('.tab-content.active').classList.remove('active');
        document.querySelector(`[data-content=${attr}-content]`).classList.add('active');
    }
});
